<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::resource('sliding_puzzle', 'SlidingPuzzleController')->only(['index', 'store', 'update', 'destroy']);
});

Route::group(['prefix' => 'account'], function () {
    Auth::routes(['logout' => false, 'profile' => false]);
});

Route::get('games', 'GameController@index');
//Route::get('games/logo/{filename}', 'GameController@logo');
Route::post('account/logout', 'Auth\LoginController@logout')
    ->middleware("auth:sanctum");
Route::post('account/me', 'Auth\ProfileController@me')
    ->middleware("auth:sanctum");
