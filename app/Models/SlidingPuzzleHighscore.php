<?php

namespace App\Models;

use App\Models\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property string puzzle_id
 * @property string user_id
 */
class SlidingPuzzleHighscore extends Model
{
    use UsesUuid;

    protected $fillable = [
        'time',
        'rows'
    ];

    /**
     * @return BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function slidingPuzzle()
    {
        return $this->belongsTo(SlidingPuzzle::class);
    }
}
