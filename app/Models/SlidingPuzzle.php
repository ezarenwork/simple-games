<?php

namespace App\Models;

use App\Models\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method static findOrFail($id)
 * @method static find($id)
 * @@property-read User owner
 * @property string user_id
 */
class SlidingPuzzle extends Model
{
    use UsesUuid;

    protected $fillable = [
        'time',
        'rows',
        'tiles'
    ];

    protected $dateFormat = 'Y-m-d H:i:s.u';

    /**
     * @return BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * @return HasMany
     */
    public function highscores()
    {
        return $this->hasMany(SlidingPuzzleHighscore::class);
    }

    /**
     * @return HasMany
     */
    public function highscoreEasy()
    {
        return $this->hasMany(SlidingPuzzleHighscore::class, 'puzzle_id')
            ->where('rows', 3)
            ->orderBy('time')
            ->take(5);
    }

    public function highscoreNormal()
    {
        return $this->hasMany(SlidingPuzzleHighscore::class, 'puzzle_id')
            ->where('rows', 4)
            ->orderBy('time')
            ->take(5);
    }

    public function highscoreHard()
    {
        return $this->hasMany(SlidingPuzzleHighscore::class, 'puzzle_id')
            ->where('rows', 5)
            ->orderBy('time')
            ->take(5);
    }

    public function highscoreExpert()
    {
        return $this->hasMany(SlidingPuzzleHighscore::class, 'puzzle_id')
            ->where('rows', 6)
            ->orderBy('time')
            ->take(5);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeWithHighscore($query)
    {
        return $query->with(['highscoreEasy', 'highscoreNormal', 'highscoreHard', 'highscoreExpert'])->first();
    }
}
