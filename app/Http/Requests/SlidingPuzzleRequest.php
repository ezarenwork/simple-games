<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property array tiles
 */
class SlidingPuzzleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function sanitize()
    {
        $this->merge(['tiles', json_encode($this->tiles)]);
        return $this->all();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->sanitize();

        return [
            'tiles' => 'required|array|max:128',
            'time' => 'required|numeric|min:0',
            'rows' => 'required|numeric|min:3|max:6'
        ];
    }
}
