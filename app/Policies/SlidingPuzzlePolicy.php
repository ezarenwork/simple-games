<?php

namespace App\Policies;

use App\Models\SlidingPuzzle;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SlidingPuzzlePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any programs.
     *
     * @param  User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the program.
     *
     * @param  User  $user
     * @param  SlidingPuzzle  $slidingPuzzle
     * @return bool
     */
    public function view(User $user, SlidingPuzzle $slidingPuzzle)
    {
        return $user->id == $slidingPuzzle->user_id;
    }

    /**
     * Determine whether the user can create programs.
     *
     * @param  User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the program.
     *
     * @param  User  $user
     * @param  SlidingPuzzle  $slidingPuzzle
     * @return bool
     */
    public function update(User $user, SlidingPuzzle $slidingPuzzle)
    {
        return $user->id == $slidingPuzzle->user_id;
    }

    /**
     * Determine whether the user can delete the program.
     *
     * @param  User  $user
     * @param  SlidingPuzzle  $slidingPuzzle
     * @return bool
     */
    public function delete(User $user, SlidingPuzzle $slidingPuzzle)
    {
        return $user->id == $slidingPuzzle->user_id;
    }

    /**
     * Determine whether the user can restore the program.
     *
     * @param  User  $user
     * @param  SlidingPuzzle  $slidingPuzzle
     * @return bool
     */
    public function restore(User $user, SlidingPuzzle $slidingPuzzle)
    {
        return $user->id == $slidingPuzzle->user_id;
    }

    /**
     * Determine whether the user can permanently delete the program.
     *
     * @param  User  $user
     * @param  SlidingPuzzle  $slidingPuzzle
     * @return bool
     */
    public function forceDelete(User $user, SlidingPuzzle $slidingPuzzle)
    {
        return $user->id == $slidingPuzzle->user_id;
    }

    /**
     * @param User $user
     * @param array $args
     * @return bool
     */
    public function upsert(User $user, array $args) {
        if (isset($args['id'])) {
            return $user->can('update', SlidingPuzzle::find($args['id']));
        } else {
            return $user->can('create', SlidingPuzzle::class);
        }
    }
}
