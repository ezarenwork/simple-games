import React, {useEffect, useRef, useState} from "react";
import {useDispatch} from "react-redux";
import Box from "@material-ui/core/Box";

import {useSlidingPuzzle} from "../store/sliding-puzzle/sliding-puzzle-selector";
import {
    createSlidingPuzzleHighscore,
    fetchSlidingPuzzleState,
    updateLocalPuzzleState,
    updateSlidingPuzzleState
} from "../store/sliding-puzzle/sliding-puzzle-actions";
import SlidingPuzzleHighscores from "../components/sliding-puzzle/highscore/SlidingPuzzleHighscoreList";
import {StyledCardBox} from "../styles/common-styles";
import FullPageLoader from "../components/common/FullPageLoader";
import {fillArray, getHighscoreTitle, shuffledTiles} from "../components/sliding-puzzle/utils";
import SlidingPuzzleTitle from "../components/sliding-puzzle/title";
import SlidingPuzzleContent from "../components/sliding-puzzle/content";


const SlidingPuzzlePage = () => {
    const dispatch = useDispatch();
    const {puzzleState} = useSlidingPuzzle();
    const {tiles, rows, id, highscore} = puzzleState;
    const timerRef = useRef();
    const [completeCombination, setCompleteCombination] = useState([]);
    const [isGameOver, setIsGameOver] = useState(false);


    useEffect(() => {
        dispatch(fetchSlidingPuzzleState())
            .then(e=> e.error && newLocalPuzzleState(resetPuzzleState));
        // eslint-disable-next-line
    }, [dispatch])

    useEffect(() => {
        setCompleteCombination(fillArray(rows));
    }, [rows])

    const newLocalPuzzleState = (newPuzzleState) => {
        dispatch(updateLocalPuzzleState(newPuzzleState));
    }

    const savePuzzleState = (tiles) => {
        dispatch(updateSlidingPuzzleState(id, {rows, tiles, time: parseInt(timerRef.current.getTime())}));
        timerRef.current.stop();
    }

    const resetPuzzleState = (rows) => {
        newLocalPuzzleState({
            tiles: shuffledTiles(rows || puzzleState.rows),
            time: 0,
            rows: rows || puzzleState.rows
        })
        timerRef.current.stop();
        timerRef.current.reset();
        setIsGameOver(false);
    }

    const gameOver = () => {
        setIsGameOver(true);
        timerRef.current.stop();
        const time = parseInt(timerRef.current.getTime());
        dispatch(createSlidingPuzzleHighscore(
            time,
            rows,
            getHighscoreTitle(rows))
        )
    }

    const SlidingPuzzle = () => {
        return (
            <Box display="flex" flexDirection="column">
                <SlidingPuzzleTitle/>
                <StyledCardBox
                    mb={3}
                    flexGrow={1}
                    display="flex"
                    flexDirection="column"
                    justifyContent="center"
                    alignItems="stretch"
                >
                    <SlidingPuzzleContent
                        timerRef={timerRef}
                        resetPuzzleState={resetPuzzleState}
                        savePuzzleState={savePuzzleState}
                        completeCombination={completeCombination}
                        gameOver={gameOver}
                        isGameOver={isGameOver}
                        newLocalPuzzleState={newLocalPuzzleState}
                    />
                </StyledCardBox>
                <StyledCardBox>
                    <SlidingPuzzleHighscores highscore={highscore}/>
                </StyledCardBox>
            </Box>
        )
    }

    return (
        <FullPageLoader loading={!tiles.length}>
            <SlidingPuzzle/>
        </FullPageLoader>
    );
}

export default SlidingPuzzlePage;

