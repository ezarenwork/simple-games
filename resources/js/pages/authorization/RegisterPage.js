import React, {useEffect} from 'react';
import {useDispatch} from "react-redux";
import {useHistory} from "react-router-dom";
import {Form, Formik} from "formik";
import * as Yup from 'yup';
import {useLocation} from "react-router";
import Box from "@material-ui/core/Box";

import {register} from "../../store/auth/auth-actions";
import {useAuth} from "../../store/auth/auth-selector";
import {AuthorizationManager} from "../../utils/authorization-manager";
import {StyledButton, StyledCardBox, StyledCardTitle, StyledNavLink} from "../../styles/common-styles";
import {CheckBoxField, TextField} from "../../styles/auth";


const initValues = {
    username: '',
    email: '',
    password: '',
    password_confirmation: ''
};

const signUpSchema = Yup.object().shape({
    username: Yup.string()
        .required()
        .min(3, 'Name is to short - should be 3 chars minimum')
        .max(20, 'Name is to long - should be 20 chars maximum'),
    email: Yup.string()
        .email("Not a valid email")
        .required(),
    password: Yup.string()
        .required('No password provided.')
        .min(6, 'Password is too short - should be 6 chars minimum.'),
    password_confirmation: Yup.string()
        .required()
        .oneOf([Yup.ref("password"), null], "Passwords must match")
});


const RegisterPage = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const location = useLocation();

    const {user} = useAuth();

    useEffect(() => {
        if (user) {
            const {from} = location.state || {from: {pathname: "/home"}};
            history.replace(from);
        }
    }, [user, history, location.state])

    return (
        <StyledCardBox width="50%" m="auto">
            <Formik
                initialValues={initValues}
                onSubmit={(values) => {
                    dispatch(register(values))
                        .then(r => {
                            if (!r.error) {
                                AuthorizationManager.setToken(r.data.access_token);
                                AuthorizationManager.setUser(r.data);
                                history.push('/home');
                            }
                        })
                }}
                validationSchema={signUpSchema}
            >{({isSubmitting, dirty, isValid}) => (

                <Form>
                    <Box
                        display="flex"
                        flexDirection="column"
                        alignItems="center"
                        width="100%"
                    >
                        <StyledCardTitle>
                            Sign Up
                        </StyledCardTitle>
                        <TextField
                            label="name"
                            name="username"
                            type="input"
                        />
                        <TextField
                            label="email"
                            name="email"
                            type="input"
                        />
                        <TextField
                            label="password"
                            name="password"
                            type="password"
                        />
                        <TextField
                            label="confirm password"
                            name="password_confirmation"
                            type="password"
                        />
                        <Box display="flex" justifyContent="space-between" width="80%">
                            <CheckBoxField
                                type="checkbox"
                                name="remember_me"
                                label="Remember Me"
                            />
                            <StyledButton disabled={isSubmitting || !dirty || !isValid} type="submit">
                                Submit
                            </StyledButton>
                        </Box>
                        <StyledNavLink to="/login">
                            <StyledCardTitle subTitle animated>
                                You already have an account?
                            </StyledCardTitle>
                        </StyledNavLink>
                    </Box>
                </Form>
            )}
            </Formik>
        </StyledCardBox>
    );
}

export default RegisterPage;