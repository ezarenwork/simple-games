export const layoutHelper = (props) => {
    const noContentPages = ["/login", "/register", "/home"];
    return noContentPages.some(p => p === props.location.pathname);
}