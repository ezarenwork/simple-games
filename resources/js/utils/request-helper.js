export const GET = 'GET';
export const POST = 'POST';
export const PUT = 'PUT';
export const DELETE = 'DELETE';
export const requestActionCreator = (type, url, method, data, meta) => ({
    type,
    payload: {
        request: {
            method,
            url,
            data
        }
    },
    meta
});