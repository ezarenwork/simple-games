import {initTheme} from '../styles/theme';

export const ThemeManager = {
    setTheme(theme) {
        localStorage.setItem('theme', JSON.stringify(theme));
    },
    getTheme() {
        const savedTheme = localStorage.getItem("theme");
        if (!savedTheme) return initTheme;
        const theme = JSON.parse(savedTheme);
        return {...initTheme, ...theme};
    }
}
