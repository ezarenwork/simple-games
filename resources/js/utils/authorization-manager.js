import axios from 'axios';

export const AuthorizationManager = {
    setToken(token) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
        localStorage.setItem("token", token);
    },
    getToken() {
        return localStorage.getItem('token');
    },
    setUser(user) {
        localStorage.setItem("user", JSON.stringify(user));
    },
    getUser() {
        const user = localStorage.getItem("user");
        if (!user) return null
        return JSON.parse(user);
    },
    clear() {
        delete axios.defaults.headers.common['Authorization'];
        localStorage.clear();
    }
}

window.onbeforeunload = () => {
    if (!AuthorizationManager.getUser()) {
        AuthorizationManager.clear();
    }
};