import { handleRequests } from '@redux-requests/core';
import { createDriver } from '@redux-requests/axios';
import createSagaMiddleware from "redux-saga";
import axios from "axios";


import {AuthorizationManager} from "../../utils/authorization-manager";


axios.defaults.baseURL = "http://127.0.0.1:8000/api/";
axios.defaults.headers.common['Authorization'] = 'Bearer ' + AuthorizationManager.getToken();

export const {requestsMiddleware} = handleRequests({
    driver: createDriver(axios),
    promisify: true
});

export const sagaMiddleware = createSagaMiddleware();
