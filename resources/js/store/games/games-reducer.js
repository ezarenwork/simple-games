import {success} from "redux-saga-requests";
import {FETCH_GAMES, FETCH_HIGHSCORES_GENERAL} from "./games-actions"

const initialState = {
    games: [],
    highscoresGeneral: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_GAMES:
        case FETCH_HIGHSCORES_GENERAL: {
            return {
                ...state
            }
        }
        case success(FETCH_GAMES): {
            return {
                ...state,
                games: action.payload.data
            };
        }
        case success(FETCH_HIGHSCORES_GENERAL): {
            return {
                ...state,
                highscoresGeneral: action.payload.highscores
            };
        }
        default:
            return state;
    }
};