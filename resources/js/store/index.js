import {applyMiddleware, combineReducers, createStore} from "redux";
import thunk from "redux-thunk";
import logger from 'redux-logger'

import {requestsMiddleware, sagaMiddleware} from "./sagas";
import games from "./games/games-reducer";
import auth from "./auth/auth-reducer"
import error from "./error/error-reducer"
import system from "./system/system-reducer"
import slidingPuzzle from "./sliding-puzzle/sliding-puzzle-reducer";
import arkanoid from "./arkanoid/arkanoid-reducer";


const reducers = combineReducers({
    system,
    games,
    auth,
    error,
    slidingPuzzle,
    arkanoid,
});

const middleware = [
    thunk,
    ...requestsMiddleware,
    sagaMiddleware
];

if (process.env.NODE_ENV === `development`) {
    middleware.push(logger);
}

const store = createStore(
    reducers,
    applyMiddleware(...middleware)
);

export default store;





