import {useSelector} from "react-redux";

export const useArkanoid = () => useSelector(state => state.arkanoid);