import {
    ADD_SYSTEM_NOTIFICATION,
    CHANGE_HEADER_MENU_STATE,
    CHANGE_THEME,
    REMOVE_SYSTEM_NOTIFICATION
} from "./system-actions";

const initialState = {
    notifications: [],
    theme: null,
    isHeaderMenuOpen: false
};

export default (state = initialState, action) => {
    switch (action.type) {
        case ADD_SYSTEM_NOTIFICATION:
            const notification = {
                ...action.payload,
                id: Math.round(Math.random() * 1e6)
            };
            return {
                ...state,
                notifications: [
                    ...state.notifications,
                    notification
                ]
            };
        case REMOVE_SYSTEM_NOTIFICATION:
            let result = [];
            if (state.notifications.length > 0) {
                result = state.notifications.filter(notification => notification.id !== action.payload.id);
            }
            return {
                ...state,
                notifications: result
            };
        case CHANGE_THEME:
            return {
                ...state,
                theme: {...state.theme, ...action.payload.theme}
            };
        case CHANGE_HEADER_MENU_STATE:
            return {
                ...state,
                isHeaderMenuOpen: action.payload.isHeaderMenuOpen
            };


        default:
            return state;
    }
};
