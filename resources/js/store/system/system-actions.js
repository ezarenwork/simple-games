export const ADD_SYSTEM_NOTIFICATION = 'ADD_SYSTEM_NOTIFICATION';
export const addSystemNotification = (title, text, type) => dispatch => {
    dispatch({
        type: ADD_SYSTEM_NOTIFICATION,
        payload: {title, text, type}
    });
}

export const REMOVE_SYSTEM_NOTIFICATION = 'REMOVE_SYSTEM_NOTIFICATION';
export const removeSystemNotification = (id) => ({
    type: REMOVE_SYSTEM_NOTIFICATION,
    payload: {id}
});

export const CHANGE_THEME = 'CHANGE_THEME';
export const changeTheme = (theme) => ({
    type: CHANGE_THEME,
    payload: {theme}
});

export const CHANGE_HEADER_MENU_STATE = 'CHANGE_HEADER_MENU_STATE';
export const changeHeaderMenuState = (isHeaderMenuOpen) => ({
    type: CHANGE_HEADER_MENU_STATE,
    payload: {isHeaderMenuOpen}
});
