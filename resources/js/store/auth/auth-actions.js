import {success} from "redux-saga-requests";

import {requestActionCreator, POST} from "../../utils/request-helper";
import {AuthorizationManager} from "../../utils/authorization-manager";

export const LOGIN = 'LOGIN';
export const login = (userData) => requestActionCreator(LOGIN, 'account/login', POST, userData);

export const REGISTER = 'REGISTER';
export const register = (userData) => requestActionCreator(REGISTER, 'account/register', POST, userData);

export const LOGOUT = 'LOGOUT';
export const logout = () => requestActionCreator(LOGOUT, 'account/logout', POST);

export const FETCH_USER = 'FETCH_USER';
export const fetchUser = () => requestActionCreator(FETCH_USER, 'account/me', POST);

export const SET_USER = 'SET_USER';
export const checkToken = user => dispatch => {
    dispatch({type: success(SET_USER), payload: {data: user}});
    dispatch(fetchUser()).then(r => r.error && AuthorizationManager.clear());
}