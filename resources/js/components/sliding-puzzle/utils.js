import moment from "moment";

export const millisecondsToHHMMSS = (milliseconds) => {
    return moment('2000-01-01 00:00:00').add(moment.duration(milliseconds)).format('HH:mm:ss');
}

export const getHighscoreTitle = (rows) => {
    return [
        {rows: 3, title: 'highscore_easy', id: Math.round(Math.random() * 1e6)},
        {rows: 4, title: 'highscore_normal', id: Math.round(Math.random() * 1e6)},
        {rows: 5, title: 'highscore_hard', id: Math.round(Math.random() * 1e6)},
        {rows: 6, title: 'highscore_expert',id: Math.round(Math.random() * 1e6)}
    ].find(title => title.rows === rows);
}

export const shuffledTiles = (rows = 4) => {
    const tiles = fillArray(rows);

    let currentIndex = tiles.length, temporaryValue, randomIndex;

    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = tiles[currentIndex];
        tiles[currentIndex] = tiles[randomIndex];
        tiles[randomIndex] = temporaryValue;
    }

    for (let i = 0; i < rows; i++) {
        tiles.splice(i, rows, tiles.slice(i, i + rows));
    }

    return tiles;
};

export const fillArray = (rows) => {
    const max = rows * rows;
    const arr = [];
    for (let i = 1; i < max; i++) {
        arr.push(i);
    }
    arr.push(0);
    return arr;
}
