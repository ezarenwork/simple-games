import React from 'react';
import Box from "@material-ui/core/Box";
import Timer from "react-compound-timer";

import {StyledContentTimer, StyledContentTime} from "../../../styles/puzzle";
import {useSlidingPuzzle} from "../../../store/sliding-puzzle/sliding-puzzle-selector";

const SlidingPuzzleTimer = ({timerRef}) => {
    const {puzzleState:{time}} = useSlidingPuzzle();

    return (
        <Box mb={2}>
            <StyledContentTime>
                <Timer ref={timerRef}
                       initialTime={time}
                       formatValue={(value) => `${(value < 10 ? `0${value}` : `${value}`)}`}
                       startImmediately={false}
                >
                    {"Time: "}
                    <StyledContentTimer>
                        <Timer.Hours/>:
                        <Timer.Minutes/>:
                        <Timer.Seconds/>
                    </StyledContentTimer>
                </Timer>
            </StyledContentTime>
        </Box>
    );
};


export default SlidingPuzzleTimer;




