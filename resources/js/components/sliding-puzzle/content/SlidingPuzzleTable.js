import React, {useState} from 'react';
import Box from "@material-ui/core/Box";
import cloneDeep from "lodash/cloneDeep";
import {useWindowSize} from 'react-use';
import Confetti from "react-confetti";

import SlidingPuzzleTableRow from "./SlidingPuzzleTableRow";
import {useSlidingPuzzle} from "../../../store/sliding-puzzle/sliding-puzzle-selector";
import {StyledButton} from "../../../styles/common-styles";


const SlidingPuzzleTable = (props) => {
    const {savePuzzleState, timer, gameOver, isGameOver, completeCombination, resetPuzzleState} = props;
    const {width, height} = useWindowSize();
    const {puzzleState} = useSlidingPuzzle();
    const [tiles, setTiles] = useState(puzzleState.tiles);

    const moveTile = (i, j) => {
        if (!isGameOver) {
            const neighbours = getNeighbours(i, j);
            const canMove = isNeighbourNull(neighbours);
            const sourceTitle = tiles[i][j];
            if (canMove) {
                timer.current.start();
                const newTiles = cloneDeep(tiles);
                if (neighbours.top === 0) {
                    newTiles[i - 1][j] = sourceTitle;
                    newTiles[i][j] = 0;
                } else if (neighbours.right === 0) {
                    newTiles[i][j + 1] = sourceTitle;
                    newTiles[i][j] = 0;
                } else if (neighbours.bot === 0) {
                    newTiles[i + 1][j] = sourceTitle;
                    newTiles[i][j] = 0;
                } else if (neighbours.left === 0) {
                    newTiles[i][j - 1] = sourceTitle;
                    newTiles[i][j] = 0;
                }
                setTiles(newTiles);
                if (checkCompletion(newTiles)) {
                    gameOver();
                }
            }
        }
    }

    const checkCompletion = puzzle => {
        return puzzle.join(",") === completeCombination.join(",");
    };

    const getNeighbours = (i, j) => {
        return {
            top: tiles[i - 1] ? tiles[i - 1][j] : undefined,
            right: tiles[i][j + 1],
            bot: tiles[i + 1] ? tiles[i + 1][j] : undefined,
            left: tiles[i][j - 1]
        }
    }

    const isNeighbourNull = (neighbours) => {
        return neighbours.top === 0 || neighbours.right === 0 || neighbours.bot === 0 || neighbours.left === 0;
    };

    return (
        <Box display="flex" flexGrow={1} flexDirection="column" justifyContent="center" alignItems="center">
            <Box mb={3}>
                <Confetti run={isGameOver} width={width} height={height}/>
                {tiles.map((row, i) => (
                    <SlidingPuzzleTableRow
                        key={i}
                        i={i}
                        isGameOver={isGameOver}
                        row={row}
                        moveTile={moveTile}
                    />
                ))}

            </Box>
            <Box display="flex" width='100%' justifyContent="space-between">
                <StyledButton onClick={() => resetPuzzleState(puzzleState.rows)}>
                    Reset Game
                </StyledButton>
                <StyledButton onClick={() => savePuzzleState(tiles)}>
                    Save Game
                </StyledButton>
            </Box>
        </Box>
    );
};

export default SlidingPuzzleTable;
