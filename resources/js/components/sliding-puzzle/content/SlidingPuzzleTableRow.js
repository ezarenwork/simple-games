import React from 'react';
import {TableRow, TableTile, TileTitle} from "../../../styles/puzzle";
import {useSlidingPuzzle} from "../../../store/sliding-puzzle/sliding-puzzle-selector";

const SlidingPuzzleTableRow = ({i, isGameOver, row, moveTile}) => {
    const {puzzleState: {rows}} = useSlidingPuzzle();

    return (
        <TableRow
            key={i}
            isComplete={isGameOver}
        >
            {row.map((col, j) => {
                return (
                    <TableTile
                        key={`${i}-${j}`}
                        isEmpty={col === 0}
                        isComplete={isGameOver}
                        onClick={() => {
                            moveTile(i, j);
                        }}
                        difficulty={rows}
                    >
                        <TileTitle difficulty={rows}>
                            {col !== 0 && col}
                        </TileTitle>
                    </TableTile>
                );
            })}
        </TableRow>
    );
};

export default SlidingPuzzleTableRow;