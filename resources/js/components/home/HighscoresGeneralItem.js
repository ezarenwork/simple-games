import React from 'react';
import {ListItemAvatar, ListItemText, Avatar, Typography, ListItem, Divider} from "@material-ui/core";


const HighscoresGeneralItem = ({highscoreGeneral}) => {
    return (
        <>
            <ListItem alignItems="flex-start">
                <ListItemAvatar>
                    <Avatar alt="Remy Sharp" src={highscoreGeneral.avatar}/>
                </ListItemAvatar>
                <ListItemText
                    primary={highscoreGeneral.game}
                    secondary={
                        <React.Fragment>
                            <Typography
                                component="span"
                                variant="body2"
                                color="textPrimary"
                            >
                                {highscoreGeneral.type}
                            </Typography>
                            {highscoreGeneral.score}
                        </React.Fragment>
                    }
                />
            </ListItem>
            <Divider variant="inset" component="li"/>
        </>
    );
};

export default HighscoresGeneralItem;