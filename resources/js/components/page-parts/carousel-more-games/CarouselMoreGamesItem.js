import React from 'react';
import Box from "@material-ui/core/Box";

import {
    StyledFlipCard,
    StyledFlipCardBack,
    StyledFlipCardFont,
    StyledIcon,
    StyledFlipCardInner
} from "../../../styles/page-parts/carousel-styles";
import {StyledNavLink} from "../../../styles/common-styles";


const CarouselMoreGamesItem = ({game}) => {
    return (
        <Box py={1}>
            <StyledNavLink to={"/" + game.name.replace(/\s+/g, '-').toLowerCase()}>
                <StyledFlipCard>
                    <StyledFlipCardInner>
                        <StyledFlipCardFont>
                            <StyledIcon image={game.logo_img}/>
                            {game.name}
                        </StyledFlipCardFont>
                        <StyledFlipCardBack>
                            <StyledIcon image={game.logo_gif}/>
                            {game.name}
                        </StyledFlipCardBack>
                    </StyledFlipCardInner>
                </StyledFlipCard>
            </StyledNavLink>
        </Box>
    );
};

export default CarouselMoreGamesItem;
