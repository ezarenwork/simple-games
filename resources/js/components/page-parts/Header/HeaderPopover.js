import React from 'react';
import {useDispatch} from "react-redux";
import Popover, {ArrowContainer} from 'react-tiny-popover'

import {logout} from "../../../store/auth/auth-actions";
import {AuthorizationManager} from "../../../utils/authorization-manager";
import SwitchTheme from "../../common/SwitchTheme";
import {useSystem} from "../../../store/system/system-selector";
import {StyledAvatar, StyledMenu, StyledMenuItem} from "../../../styles/page-parts/header-styles";
import {changeHeaderMenuState} from "../../../store/system/system-actions";
import {useAuth} from "../../../store/auth/auth-selector";
import {StyledCardBox} from "../../../styles/common-styles";
import Divider from "@material-ui/core/Divider";

const HeaderPopover = () => {
    const dispatch = useDispatch();
    const {isHeaderMenuOpen} = useSystem();
    const {user} = useAuth();
    const {theme} = useSystem();

    const handleLogout = () => {
        dispatch(logout())
            .then(() => {
                AuthorizationManager.clear();
                window.location.reload();
            });
    };


    return (
        <Popover
            isOpen={isHeaderMenuOpen}
            position={'bottom'}
            align={'end'}
            padding={20}
            disableReposition
            transitionDuration={0}
            onClickOutside={() => dispatch(changeHeaderMenuState(false))}
            content={props => (
                <ArrowContainer
                    position={props.position}
                    targetRect={props.targetRect}
                    popoverRect={props.popoverRect}
                    arrowSize={10}
                    arrowColor={theme.palette[theme.mode].card.background}
                    arrowStyle={{ opacity: 0.7 }}
                >
                    <StyledCardBox>
                    <StyledMenu>
                        <StyledMenuItem><SwitchTheme/></StyledMenuItem>
                        <Divider/>
                        <StyledMenuItem>Profile</StyledMenuItem>
                        <Divider/>
                        <StyledMenuItem onClick={handleLogout}>Logout</StyledMenuItem>
                    </StyledMenu>
                    </StyledCardBox>
                </ArrowContainer>
            )}
        >
            <StyledAvatar
                onClick={() => dispatch(changeHeaderMenuState(!isHeaderMenuOpen))}
                src={user.avatar || ''}
                alt='avatar'/>
        </Popover>
    )
}

export default HeaderPopover;
