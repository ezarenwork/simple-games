import React from 'react';
import PacmanLoader from 'react-spinners/PacmanLoader'

import {StyledLoaderContainer, StyledLoadingOverlay} from "../../styles/common/loader";
import {useError} from "../../store/error/error-selector";
import {useSystem} from "../../store/system/system-selector";


const FullPageLoader = ({loading, children}) => {
    const {error} = useError();
    const {theme} = useSystem();


    return (
        <>
            {
                loading && !error
                    ? <StyledLoaderContainer>
                        <StyledLoadingOverlay>
                            <PacmanLoader
                                color={theme ? theme.palette[theme.mode].loader.background : '#000000'}
                                size={40}
                            />
                        </StyledLoadingOverlay>
                    </StyledLoaderContainer>
                    : <>{children}</>
            }
        </>
    );
};

export default FullPageLoader;
