import React from 'react';
import {
    StyledWrapper,
    StyledCube,
    StyledFront,
    StyledBack,
    StyledTop,
    StyledBottom,
    StyledLeft,
    StyledRight,
    StyledPerspective
} from "../../styles/common/cube";
import {useGames} from "../../store/games/games-selector";

export const LogoCubeAnimation = () => {
    const {games} = useGames();
    const isReady = games.length > 0;
    const url = (index) => {
         return games[index].logo_gif || games[index].logo_img;
    }

    return (
        <StyledPerspective>
            <StyledWrapper>
                {
                    isReady &&
                    <StyledCube>
                        <StyledFront image={url(0)}/>
                        <StyledBack image={url(0)}/>
                        <StyledTop image={url(1)}/>
                        <StyledBottom image={url(1)}/>
                        <StyledLeft image={url(2)}/>
                        <StyledRight image={url(2)}/>
                    </StyledCube>
                }
            </StyledWrapper>
        </StyledPerspective>
    );
};
