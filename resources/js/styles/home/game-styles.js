import styled from "styled-components";
import {CardContent} from "@material-ui/core";
import {StyledCardBox} from "../common-styles";


export const StyledGameCard = styled(StyledCardBox)`
   transition: transform .4s ease-out;
     &:hover {
       transform: scale(1.03);
     }
`;

export const StyledCardImage = styled.div`
   height: ${props => props.height || '240px'};
   width: ${props => props.width || '240px'};
   background-image: url(${props => props.image});
   background-size: cover;
   &:hover{
      background-image: url(${props => props.gif});
   }
`;

export const StyledCardContent = styled(CardContent)`
   display: flex;
   justify-content: center;
   padding: 0.8rem 0 0 0  !important;
`;
