import styled from "styled-components";
import {ToastContainer} from "react-toastify";

export const StyledToastContainer = styled(ToastContainer)` 
    font-size: 20px;
    opacity: .5;
`;