import {createGlobalStyle} from "styled-components";


const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
    &:focus{
       outline: none !important;
     }
  }
  body {
    background-color: ${({theme:{palette, mode}}) => palette[mode].root.background};
    color:  ${({theme:{palette, mode}}) => palette[mode].root.color};
    font-size: 10px;
    font-family: 'Permanent Marker', cursive;
    overflow-x: hidden;
  }
`;

export default GlobalStyle;
