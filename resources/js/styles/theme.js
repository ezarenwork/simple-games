export const initTheme = {
    mode: 'light',
    palette: {
        light: {
            root: {
                color: "#5e6472",
                background: "#fffffc",
                boxShadow: "0 2px 5px 0 rgba(0,0,0,0.32)"
            },
            card: {
                background: "#f6f6f6",
            },
            header: {
                background: "#e3e3e3",
            },
            button: {
                backgroundDisabled: 'rgba(38,38,38,0.1)',
                colorDisabled: '#b76b6c',
            },
            title: {
                background: "#e5e5e5",
            },
            table: {
                line: {
                    background: "#d7d7d7",
                }
            },
            loader: {
                background: "#222222",
            },
            puzzle: {
                tile: {
                    background: "#ec898c",
                    backgroundComplete: "#b4696b",
                }
            },
            arkanoid: {
                canvas: {
                    background: '#4999d8'
                }
            }
        },
        dark: {
            root: {
                color: "#c8d0d2",
                background: "#171717",
                boxShadow: "0 2px 5px 0 rgba(0,0,0)"
            },
            header: {
                background: "#333333",
            },
            card: {
                background: "#222222",
            },
            button: {
                backgroundDisabled: 'rgba(52,52,52,0.8)',
                colorDisabled: '#b76b6c',
            },
            title: {
                background: "#343434",
            },
            table: {
                line: {
                    background: "#474747",
                }
            },
            loader: {
                background: "#e5e5e5",
            },
            puzzle: {
                tile: {
                    background: "#955658",
                    backgroundComplete: "#633b3d",
                }
            },
            arkanoid: {
                canvas: {
                    background: '#30678e'
                }
            }
        }
    }
}
