import styled from "styled-components";
import Box from "@material-ui/core/Box";

export const StyledCanvasBox = styled(Box)`
    &&{
     background-color: ${({theme: {palette, mode}}) => palette[mode].arkanoid.canvas.background};
    }
`;
