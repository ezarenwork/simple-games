<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Game;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


$factory->define(Game::class, function (Faker $faker) {

    static $gameIndex = 0;
    $gameNames = ['Sliding Puzzle', 'Arkanoid', 'Tic Tac Toe', 'Snake', 'Dots', 'Naval Battle'];

    /*
     * Get all files from aws s3
     */
    $s3 = Storage::disk('s3');
    $diskFiles = $s3->files();
    $imgUrls = [];
    foreach ($diskFiles as $diskFile) {
        if ($s3->exists($diskFile)) {
            $command = $s3->getDriver()->getAdapter()->getClient()->getCommand('GetObject', [
                'Bucket' => config('filesystems.disks.s3.bucket'),
                'Key' => $diskFile
            ]);
            $request = $s3->getDriver()->getAdapter()->getClient()->createPresignedRequest($command, '+5 minutes');
            $imgUrls[] = (string)$request->getUri();
        }
    }

    /*
     * Partition array with all files to arrays with gis and array with jpg
     */
    $logoFiles = [];
    $gifFiles = [];
    foreach ($imgUrls as $imgUrl) {
        $extWithArgs = File::extension($imgUrl);
        $ext = strstr($extWithArgs, '?', true);
        if ($ext != "gif") {
            $logoFiles[] = strstr($imgUrl, '?', true);
        } else {
            $gifFiles[] = strstr($imgUrl, '?', true);
        }
    }

    return [
        'name' => $gameNames[$gameIndex],
        'description' => $faker->paragraph,
        'logo_img' => $logoFiles[$gameIndex],
        'logo_gif' => $gifFiles[$gameIndex++],
    ];
});
